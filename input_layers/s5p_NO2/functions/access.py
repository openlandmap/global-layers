import pandas as pd
import rasterio
from eumap import parallel
from pathlib import Path
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

S3_URL = 'https://s3.openlandmap.org/arco'

def _eval(str_val, args):
    return eval("f'"+str_val+"'", args)

def _gen_urls(layer, start_year_month, end_year_month, base_url=S3_URL):
    urls = []
    dt_format = '%Y%m%d'
    for month in pd.date_range(start_year_month, end_year_month,freq='M'):
        dt1 = f"{month.strftime('%Y%m')}01"
        dt2 = f"{month.strftime('%Y%m%d')}"
        url = _eval(f'{base_url}/{layer}', locals())
        urls.append(url)
    return urls

def get_sp5_no2_monthlyaggr_urls(start_year_month='5/2018', end_year_month='11/2022'):
    return _gen_urls('no2_s5p.l3.trop.tmwm_p50_2km_a_{dt1}_{dt2}_go_epsg.4326_v20221219.tif', start_year_month, end_year_month)

def get_sp5_no2_overall_urls(percentile_list = ['p10','p50','p90'], base_url=S3_URL):
    options = ['p10','p50','p90']
    target_percentile = [i for i in percentile_list if i in options]
    off_target = [i for i in percentile_list if i not in options]
    if off_target != []:
        print(f'{off_target} is not available, please specify only {options[0],options[1],options[2]}')
    urls = []
    for percentile in target_percentile:
        url = _eval(f'{base_url}/no2_s5p.l3.trop.tmwm_p50.{percentile}_2km_a_201805_202211_go_epsg.4326_v20221219.tif',locals())    
        urls.append(url)
    return  urls

def read_overview(layer_path, oviews_pos = -4, verbose = False):
    if verbose:
        print(f'Reading {layer_path}')
    with rasterio.open(layer_path) as src:
        # List of overviews from biggest to smallest
        oviews = src.overviews(1)
        if verbose:
            print(f'-Available overviews: {oviews}')
        oview = oviews[oviews_pos]
        result = src.read(1, out_shape=(1, src.height // oview, src.width // oview)).astype('float32')
        return result, src

def read_pixel(cog_url, coordinates):
    pixel_val = None
    try:
        with rasterio.open(cog_url) as ds:
            pixel_val = np.stack(ds.sample(coordinates))
    except Exception as e:
        print(e, 'at coordinates', coordinates)
    return pixel_val

def read_ts(lon, lat, urls):
    result = []
    args = [ (cog_url, [ (lon, lat) ] ) for cog_url in urls ]
    for arg, pixel_vals in zip(args, parallel.job(read_pixel, args, n_jobs=-1)):
        raster_name = Path(arg[0]).name
        cordinates = arg[1]
        try:
            start_date = str(raster_name).split('_')[5]
            end_date = str(raster_name).split('_')[6]
        except:
            dt = None
            start_date = None
            end_date = None
        
        #print(raster_name, pixel_vals)
        for i in range(0, len(cordinates)):
            lon, lat = cordinates[i]
            result.append({
                'raster_name': raster_name,
                'start_date': start_date,
                'end_date': end_date,
                'lon': lon,
                'lat': lat, 
                'value': float(pixel_vals[i][0])
            })

    return pd.DataFrame(result)

def read_and_plot_ts(lat, lon, start_year_month='5/2018', end_year_month='11/2022'):
    gpf = read_ts(lon, lat, get_sp5_no2_monthlyaggr_urls(start_year_month, end_year_month))

    sns.set_style("ticks")
    matplotlib.rcParams.update({'font.size': 15})

    myfig, myax = plt.subplots(figsize=(20, 6))

    # Plot temperature
    myax.plot(pd.to_datetime(gpf['start_date'], format='%Y-%m-%d'), gpf['value']/10, color='tab:blue', linestyle='-', label='Gapfilled')

    myax.set_xlabel('Time')
    myax.set_ylabel('NO2 density (µmol m^2)')
    myax.set_title(f'Sp5-NO2 ({lon:.4f}, {lat:.4f})')
    myax.grid(False)

    # format x axis labels
    #myax.xaxis.set_major_locator(DayLocator())
    #myax.xaxis.set_major_formatter(mdates.DateFormatter('%y%m%d'))
    fmt_half_year = mdates.MonthLocator(interval=29)

    myax.legend(loc='lower left')